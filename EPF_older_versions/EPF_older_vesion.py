from __future__ import print_function, division
import os
import pandas as pd
import numpy as np
import gurobipy as gb
import networkx as nx
import sys
from multiprocessing import Pool

def drop_duplicates_from_index(self,keep='first',inplace=False):
    """Return self with duplicate indices removed
    Parameters
    ----------
    keep : {'first_last', first', 'last', False}, default 'first'
        - ``first_last`` : Drop duplicates except for the first and the last occurence
        - otherwise default 'drop_duplicates' behaviour.
    """
    if keep=='first_last':
        result = self[np.logical_not(self.index.duplicated(keep='first')) | np.logical_not(self.index.duplicated(keep='last'))]
    else:
        result = self[np.logical_not(self.index.duplicated(keep=keep))]
    if inplace:
        return self._update_inplace(result)
    else:
        return result
pd.Series.drop_duplicates_from_index = drop_duplicates_from_index


# load graph data
nodes = pd.read_csv("data/graph/nodes14.csv",header=None,squeeze=True).values
edges = pd.read_csv("data/graph/edges.csv",header=None).values

index = []
for n, edge in enumerate(edges):
    if edge[0] in nodes and edge[1] in nodes:
        index.append(True)
    else:
        index.append(False)
edges = edges[index]
network = nx.Graph()
network.add_nodes_from(nodes)
network.add_edges_from(edges)
nodes = network.nodes()
edges = network.edges()

# load flow constraints
fl_b = pd.read_csv('./data/graph/flow_constraints_14.csv',header=0,index_col=0)
Fl_b = []
for n, edge in enumerate(edges):
    try:
        l = edge[0]+'-'+edge[1]
        Fl_b.append(fl_b.loc[l,'BC'])
    except KeyError:
        l = edge[1]+'-'+edge[0]
        Fl_b.append(fl_b.loc[l,'BC'])


# assume link susceptances are equal
link_susceptances = np.ones(len(network.edges()))

# the dimension of incidence matrix is n*l
K = (-nx.incidence_matrix(network,oriented=True)).toarray()
O = np.diag(link_susceptances)
B_inverse = np.linalg.pinv((K.dot(O)).dot(K.T))

# H = O*K^T*B^-1, H is used for simplified AC power flow
H = (O.dot(K.T)).dot(B_inverse)
H[abs(H)<1e-13]=0

# load Cost-Response data from EnergyPLAN
CB = {}
for n in nodes:
    df = pd.read_csv("data/Scenarios/{}15.csv".format(n),index_col=0,sep=';')
    df.columns = pd.to_numeric(df.columns.values)
    # convert from MWh to GWh
    CB[n] = df/1e3

epsilon = 1e-3
inf = gb.GRB.INFINITY


# function EPPF receives a time step, perform the power flow optimization, 
# then return the result in a pandas Series, named by the time step (t-1)
def EPPF(t):
    # initialize the gurobi model
    m = gb.Model()
    m.setParam('OutputFlag', 0)
    m.Params.Threads=1 

    # create the variable called total flow, positive indicates export and negative corresponds to import
    fl_t = [m.addVar(lb=-Fl_b[n],ub=Fl_b[n],name='link {}-{}'.format(edge[0],edge[1])) for n,edge in enumerate(edges)]
    # for unconstrained links
    #fl_t = [m.addVar(lb=-inf,ub=inf,name='link {}-{}'.format(edge[0],edge[1])) for n,edge in enumerate(edges)]
    
    Obj = gb.QuadExpr()
    Obj.addTerms(np.repeat(epsilon,len(edges)),fl_t,fl_t)

    # add fl_t^2 to the objective function, weighted by epsilon
    m.setObjective(expr=Obj)

    b = {}
    p = {}
    lbs =[]
    for n in nodes:
        b[n] = CB[n].loc[t,:].values.tolist()
        lbs.append(b[n][0])
        a = pd.Series(index=b[n],data = CB[n].columns.values.tolist())
        a = a.drop_duplicates_from_index(keep='first_last')
        a.sort_index(inplace=True)
        b[n] = a.index.values
        # add 1MW to the largest response value, incase that all the response values are equal
        b[n][-1] = b[n][-1]+0.001
        p[n] = a.values

    # add two more variables, namely injection and curtailment
    P = [m.addVar(lb=-inf, name='injection {}'.format(node)) for node in nodes]
    C = [m.addVar(lb=0., name='curtailment {}'.format(node)) for node in nodes]
    
    # create the variable called Response
    R = [m.addVar(lb=lb, name='response {}'.format(node)) for node,lb in zip(nodes,lbs)]
    m.update()
    
    # add piecewise linear objective function, 
    [m.setPWLObj(R[nn],b[node],p[node]*(b[node]-b[node][0])) for nn,node in enumerate(nodes)]   
    [m.addConstr(R[nn]-P[nn]-C[nn] == 0) for nn in range(len(nodes))]
    [m.addConstr(P[nn]-gb.LinExpr(K[nn],fl_t) == 0) for nn in range(len(nodes))]

    m.optimize()

    # store the result into a dataframe
    d = {}
    for v in m.getVars():
        d[v.VarName] = v.X
    d['objective'] = m.ObjVal

    # interpolate the cost for each country based on the optimization output
    for n in nodes:
        if d['response '+n]-b[n][0] == 0:
            d['cost '+n] = 0
        else:
            d['cost '+n] = np.interp(d['response '+n],b[n],p[n])

    return pd.Series(d,name=t)


# solve the optimization problem by multi-cores
cores = 2
p = Pool(cores)
T = int(sys.argv[1])
r = p.map(EPPF,range(1,T+1))
results = pd.DataFrame()
for i in range(T):
    results = pd.concat([results,r[i]],axis=1)
results = results.T.round(3)


# store the result seperately in 
# C(curtailment), P(injection), R(response), L(links), O(objective value)
d = {'C':'curtailment',
     'R':'response',
     'P':'injection',
     'L':'link',
     'O':'objective',
    }
d_df = {}
for key,value in d.items():
    d_df[key] = results.filter(like=value)
    d_df[key].to_csv('results/{}.csv'.format(value),sep=';')

for n in nodes:
    results.loc[:,'cost '+n].to_csv('results/costs/{}.csv'.format(n),sep=';',header=True)

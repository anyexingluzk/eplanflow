import numpy as np
import gurobipy as gb

# Test of 3 nodes for EnergyPLAN power flow

inf = gb.GRB.INFINITY
epsilon = 1e-5

network = gb.Model()

nodes = ['AT','CH','DE']
edges = [('AT','CH'),('CH','DE'),('DE','AT')]

Bn_a = [0.5, 1, 2]
Bn_lb = [-2, -1, 1]

# DEFINE VARIABLES
# B variables
Bn = [network.addVar(name='B {}'.format(node), lb=lb) for node,lb in zip(nodes,Bn_lb)]

# flow variables
fl = [network.addVar(lb=-2,name='link {}-{}'.format(bus0,bus1)) for bus0,bus1 in edges]
fl_pos = [network.addVar(name='link {}-{} pos'.format(bus0,bus1)) for bus0,bus1 in edges]
fl_neg = [network.addVar(name='link {}-{} neg'.format(bus0,bus1)) for bus0,bus1 in edges]

# register gurobi variables in the model
network.update()

# DEFINE CONSTRAINTS
# f = f_pos - f_neg
[network.addConstr(fltot - (flp-fln) == 0) for fltot,flp,fln in
        zip(fl,fl_pos,fl_neg)]

# Nodal energy balance constraint
# B = sum_l K_nl*fl
for nn,node in enumerate(nodes):
    network.addConstr(Bn[nn] - (
        gb.quicksum([fl[ll] for ll,edge in enumerate(edges) if edge[0]==node]) +
        gb.quicksum([-fl[ll] for ll,edge in enumerate(edges) if edge[1]==node])
        ) == 0 ,'balance-{}'.format(node)) 

# SET OBJECTIVE
Obj = gb.LinExpr(Bn_a,Bn)

Obj.addTerms(np.repeat(epsilon,2*len(edges)),fl_pos+fl_neg)

network.setObjective(expr=Obj,sense=gb.GRB.MINIMIZE)

# OPTIMIZE
network.optimize()

# EXTRACT RESULTS
print(Bn)
print(fl)


# coding: utf-8

# # EPF is short for 'EnergyPLAN Power Flow'
# ## It attempts to perform power flow optimization based on outputs from EnergyPLAN.
# ## It leans heavily on some packages, namely gurobi, networkx, pandas.

# In[1]:


from __future__ import print_function, division
import os
import math
import pandas as pd
import numpy as np
from gurobipy import *
import gurobipy as gb
import networkx as nx
from multiprocessing import Pool
from itertools import product
from decimal import *
import matplotlib.pyplot as plt
import time


# ## The following function drops superfluous data points, to avoid warning from Gurobi

# In[2]:


def drop_duplicates_from_index(self,keep='first',inplace=False):
    """Return self with duplicate indices removed
    Parameters
    ----------
    keep : {'first_last', first', 'last', False}, default 'first'
        - ``first_last`` : Drop duplicates except for the first and the last occurence
        - otherwise default 'drop_duplicates' behaviour.
    """
    if keep=='first_last':
        result = self[np.logical_not(self.index.duplicated(keep='first')) | np.logical_not(self.index.duplicated(keep='last'))]
    else:
        result = self[np.logical_not(self.index.duplicated(keep=keep))]
    if inplace:
        return self._update_inplace(result)
    else:
        return result
pd.Series.drop_duplicates_from_index = drop_duplicates_from_index


# In[3]:


def create_dir(dirName):# Create target Directory if don't exist
    if not os.path.exists(dirName):
        os.mkdir(dirName)
        print("Directory " , dirName ,  " Created ")
    else:    
        print("Directory " , dirName ,  " already exists")


# In[4]:


def remove_constraints(m):
    removed = []

    # Loop until we reduce to a model that can be solved
    while True:

        m.computeIIS()
        for c in m.getConstrs():
            if c.IISConstr:
                # Remove a single constraint from the model
                removed.append(str(c.constrName))
                m.remove(c)
                break

        m.optimize()
        status = m.status

        if status == GRB.Status.UNBOUNDED:
            print('The model cannot be solved because it is unbounded')
            exit(0)
        if status == GRB.Status.OPTIMAL:
            break
        if status != GRB.Status.INF_OR_UNBD and status != GRB.Status.INFEASIBLE:
            print('Optimization was stopped with status %d' % status)
            exit(0)
            
    return m


# ## Load data, calculate incidence matrix and PTDF matrix

# In[5]:


# load graph data
nodes = pd.read_csv("data/graph/nodes14.csv",header=None,squeeze=True).values
edges = pd.read_csv("data/graph/edges.csv",header=None).values

index = []
for n, edge in enumerate(edges):
    if edge[0] in nodes and edge[1] in nodes:
        index.append(True)
    else:
        index.append(False)
edges = edges[index]
network = nx.Graph()
network.add_nodes_from(nodes)
network.add_edges_from(edges)
nodes = network.nodes()
edges = network.edges()


# In[6]:


# load flow constraints
fl_b = pd.read_csv('data/graph/line_capacity_14.csv',header=0,index_col=0)
Fl_b = []
for n, edge in enumerate(edges):
    try:
        l = edge[0]+'-'+edge[1]
        Fl_b.append(fl_b.loc[l,'line capacity']) # in GW
    except KeyError:
        l = edge[1]+'-'+edge[0]
        Fl_b.append(fl_b.loc[l,'line capacity']) # in GW


# In[7]:


# assume link susceptances are equal
link_susceptances = np.ones(len(network.edges()))

# the dimension of incidence matrix is n*l
K = (-nx.incidence_matrix(network,oriented=True)).toarray()
O = np.diag(link_susceptances)
B_inverse = np.linalg.pinv((K.dot(O)).dot(K.T))

# H = O*K^T*B^-1, H is used for simplified AC power flow
H = (O.dot(K.T)).dot(B_inverse)
H[abs(H)<1e-13]=0

# load Cost-Response data from EnergyPLAN
CB = {}
for n in nodes:
    
    #df = pd.read_csv("data/Scenarios/{}15.csv".format(n),index_col=0,sep=';')
    df = pd.read_csv('results/EnergyPLAN/iteration1/{}2015.txt.txt'.format(n),sep='\t',header=None)
    df.columns = df.iloc[0]
    df = df.reindex(df.index.drop(0))
    df = df.dropna(axis=1)

    # convert from MWh to GWh
    CB[n] = df

inf = gb.GRB.INFINITY 


# ## Perform the flow optimization for each time step independently, and store the result in a pandas dataframe

# In[16]:


# function EPPF receives a time step, perform the power flow optimization, 
# then return the result in a pandas Series, named by the time step t
def EPPF(t,first_run=True,epsilon=1e-5):
    # initialize the gurobi model
    m = gb.Model()
    m.setParam('OutputFlag', 0)
    m.Params.Threads=1 

    # create the variable called total flow, positive indicates export and negative corresponds to import
    fl_t = [m.addVar(lb=-Fl_b[n],ub=Fl_b[n],name='link {}-{}'.format(edge[0],edge[1])) for n,edge in enumerate(edges)]
#     fl_t = [m.addVar(lb=-inf,ub=inf,name='link {}-{}'.format(edge[0],edge[1])) for n,edge in enumerate(edges)]

    Obj = gb.QuadExpr()
    Obj.addTerms(np.repeat(epsilon,len(edges)),fl_t,fl_t)

    # add fl_t^2 to the objective function, weighted by epsilon
    m.setObjective(expr=Obj)

    # b is the response value and p is the corresponding cost
    b = {}
    p = {}
    lbs = []
    ubs = []
    
    for n in nodes:
        if first_run == True:
            b[n] = CB[n].loc[t,:].values.tolist()
            a = pd.Series(index=b[n],data = CB[n].columns.values.tolist())
        else:
            b[n] = CB[n].loc[t,1::2].values.tolist()
            a = pd.Series(index=b[n],data = CB[n].loc[t,::2].values.tolist())
        lbs.append(b[n][0])
        ubs.append(b[n][-1])
        a = a.drop_duplicates_from_index(keep='first_last')
        a.sort_index(inplace=True)
        b[n] = a.index.values
        # add 1MW to the largest response value, incase that all the response values are equal
        b[n][-1] += 1
        p[n] = a.values

    # add two more variables, namely injection and curtailment
    P = [m.addVar(lb=-inf, name='injection {}'.format(node)) for node in nodes]
    C = [m.addVar(lb=0., name='curtailment {}'.format(node)) for node in nodes]

    # create the variable called Response
#     R = [m.addVar(lb=lb,ub=ub,name='response {}'.format(node)) for node,lb,ub in zip(nodes,lbs,ubs)]
    R = [m.addVar(lb=lb,name='response {}'.format(node)) for node,lb in zip(nodes,lbs)]
    m.update()

    # add piecewise linear objective function 
    # Other objective values are linearly interpolated between neighboring points.
    # The $x$ entries must appear in non-decreasing order. Two points can have the same $x$ coordinate. 
    [m.setPWLObj(R[nn],b[node],p[node]*(b[node]-b[node][0])) for nn,node in enumerate(nodes)]   
    [m.addConstr(R[nn]-P[nn]-C[nn] == 0) for nn in range(len(nodes))]
    [m.addConstr(P[nn]-gb.LinExpr(K[nn],fl_t) == 0) for nn in range(len(nodes))]

    m.optimize()
    
    status = m.status
    if status == GRB.Status.INFEASIBLE:
        # do IIS
        print('The model at hour {} is infeasible; computing IIS to remove some constraints.'.format(t))
        remove_constraints(m)

    # store the result into a dataframe
    d = {}
    for v in m.getVars():
        d[v.VarName] = v.X
    d['objective'] = m.ObjVal

    # interpolate the cost for each country based on the optimization output
    for n in nodes:
        if math.isclose(d['response '+n], b[n][0],abs_tol=0.01):
            d['cost '+n] = p[n][0]
        else:
            d['cost '+n] = np.interp(d['response '+n],b[n],p[n])
    return pd.Series(d,name=t)


# In[9]:


# solve the optimization problem by multi-cores
T = 24*366
d_results = {}
cores = 2
p = Pool(cores)

# use starmap for multiple arguments
# r = p.starmap(EPPF,product(range(1,T+1),[True]))
r = p.map(EPPF,range(1,T+1))
print('EnergyPLAN Power Flow optimization is done.')


# In[10]:


# save the results in a dict of dataframe
results = pd.DataFrame(r).astype(int)
iteration_index = 1
d_results[iteration_index] = results

# store the result seperately in 
# C(curtailment), P(injection), R(response), L(links), O(objective value)
d = {'C':'curtailment',
     'R':'response',
     'P':'injection',
     'O':'cost',
    }

create_dir('results/EPF/iteration{}'.format(iteration_index))
for v in d.values():
    create_dir('results/EPF/iteration{}/{}'.format(iteration_index,v))
    for n in nodes:    
        results.loc[:,v+' '+n].to_csv('results/EPF/iteration{}/{}/initalize_{}.txt'.format(iteration_index,v,n),sep=';',index=False)
print('Results saved.')

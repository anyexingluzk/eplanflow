\documentclass[a4paper, 11pt]{scrartcl}
\setkomafont{disposition}{\normalfont\bfseries}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{microtype}
\usepackage[margin=3cm]{geometry}
\usepackage{lipsum}
\usepackage{url}
\usepackage{graphicx}
\usepackage{cite}
\usepackage[hidelinks]{hyperref}
\usepackage{amsmath}
\usepackage[draft]{fixme}
\usepackage{pgfgantt}
\usepackage{subfig}
\usepackage[sc]{mathpazo}
\usepackage{float}

\linespread{1.05}
\parindent 0pt
\parskip 4pt

\usepackage{booktabs}
\setlength{\heavyrulewidth}{0.15em}
\setlength{\lightrulewidth}{0.08em}

% Where to look for figures
\graphicspath{{./figures/}}

% Macros
\newcommand{\sref}[1]{Section~\ref{#1}}
\newcommand{\fref}[1]{Figure~\ref{#1}}
\newcommand{\tref}[1]{Table~\ref{#1}}
\renewcommand{\eqref}[1]{(\ref{#1})}


% Title stuff
\title{EnergyPLAN Power Flow Optimization}
\subtitle{}
\author{Kun Zhu\\\href{mailto:kunzhu@eng.au.dk}{kunzhu@eng.au.dk} \\
		David Peter Schlachtberger\\\href{mailto:schlachtberger@fias.uni-frankfurt.de}{schlachtberger@fias.uni-frankfurt.de}}
\date{\today}

%----------------------------------------------------------------------------------------

\begin{document}
\maketitle
\setcounter{tocdepth}{2}
The following document describes power flow optimization based on outputs from EnergyPLAN. 

\section{Methodology}
\subsection{Nodal Response}
EnergyPLAN can calculate nodal response (net import or export plus curtailment) denoted by $R_n(t)$ at a given electricity price $c_n(t)$ on the interconnector to an external market. $R_n(t)\leq0$ means that the node would prefer to import this amount of electricity at the given price $c_n(t)$ from an external market instead of using its own expensive generators, while $R_n(t)>0$ stands for export to share cheap electricity or curtailment if line capacity is limited. Be aware that this response pattern includes curtailment, which is different from the injection pattern $P_n(t)$ (net import or export)
\begin{equation*}
P_n(t)=E_n(t)-I_n(t)
\end{equation*}
where $I_n(t)\geq 0$ denotes import and $E_n(t)\geq 0$ denotes export. As a consequence, $R_n(t)$ and $P_n(t)$ have to fulfill the following equation
\begin{equation}
R_n(t) = P_n(t)+C_n(t)
\label{response_injection_equation}
\end{equation}
where $C_n(t)$ is the curtailment required to fulfill the nodal balance. In each time step, $R_n(t)$ is determined by a fixed price on the interconnector. Figure \ref{renewable_deficit} and \ref{renewable_excess} show two typical profiles of the cost-response relation, assuming they are two nodes. Figure \ref{renewable_deficit} indicates that node 'A' will import 2000~MWh when it is offered at a price of 0~Eur/MWh, while it will export 4000~MWh when the market offers a price of 40~Eur/MWh. If the marginal cost of renewable generation is assumed to be 0~Eur/MWh and back-up generators to be non-zero, then node 'A' corresponds to a deficit case between load and renewable generations. On the other hand, node 'B' in Figure \ref{renewable_excess} will export 1000~MWh even at a price of 0~Eur/MWh, and it will export more at a higher given price. Contrary to node 'A', node 'B' has renewable excess at this time step. 

\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{RES_deficit.pdf}
	\caption{Marginal cost and nodal response relation for a node with renewable deficit.}
	\label{renewable_deficit}
\end{figure}
\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{RES_excess.pdf}
	\caption{Marginal cost and nodal response relation for a node with renewable excess.}
	\label{renewable_excess}
\end{figure}

Together, the cost-response time series and the network topology form a flow problem, where we want to perform optimization. Since response is related to the flow, we choose the response pattern $R_n(t)$ as the variable of the optimization problem, and we assume that the marginal cost in each node $c_n(t)$ is determined by $R_n(t)$, which can be generalized by the following equation
\begin{equation*}
c_n(t) = f(R_n(t))
\label{response_cost_equation}
\end{equation*}
$R_n(t)$ has to fulfill the constraint
\begin{equation}
R_n(t) \geq f^{-1}(c_n(t)=0)=R^0_n(t)
\label{response_lower_bound}
\end{equation}
where $R^0_n(t)$ is the response value when the cost is equal to 0~Eur/MWh. It implies the mismatch between load and renewable generation. For instance, $R^0_n(t)=-2000$~MWh for node 'A', while $R^0_n(t)=1000$~MWh for node 'B'. 
\subsection{Objective Function}
Now let's assume the node $n$ has response value of $R_n(t)$ at time step $t$ and the corresponding marginal price will be $f(R_n(t))$. Due to the discrepancy between $R_n(t)$ and $R^0_n(t)$, node n has to cover the amount of $(R_n(t)-R^0_n(t))$ at a cost of $c_n(t)=f(R_n(t))$ inside node n. Let's take node 'A' in Figure \ref{renewable_deficit} as an example. If $R_n(t)=-2000$~MWh, then node 'A' doesn't need to use back-up generators since it's already balanced and the nodal cost of 'A' is 0~Eur. However, if $R_n(t)=-1500$~MWh, then node 'A' has to cover the residual load $(R_n(t)-R^0_n(t))=500$~MWh on its own at a cost of $f(R_n(t))=f(-1500)\geq10$~Eur/MWh. In such case, the nodal cost of 'A' becomes $(R_n(t)-R^0_n(t))\cdot c_n(t)\geq500\text{~MWh}\cdot 10\text{~Eur/MWh}=5000$~Eur. If node 'A' decides to export 4000~MWh due to the external market, the nodal cost becomes $6000\text{~MWh}\cdot40\text{~Eur/MWh}=240000\text{~Eur}$ for node 'A'. In general, the nodal cost of node n can be calculated as
\begin{equation*}
\begin{split}
\text{Nodal cost of node n} 
& = (R_n(t)-R^0_n(t))\cdot c_n(t)\\
& = (R_n(t)-R^0_n(t))\cdot f(R_n(t))
\end{split}
\end{equation*}
It can be clearly seen that by varying $R_n(t)$, we get different nodal costs for each node. Our goal is to minimize the total system cost, which implies maximization of the renewable usage and reduction of back-up generation throughout the network. As a consequence, we choose the sum of nodal cost as the objective function for the optimization problem
\begin{equation}
\underset{R_n(t)}{\text{minimize}} \ \ 
\sum_n (R_n(t)-R^0_n(t))\cdot f(R_n(t))
\end{equation}
To solve this optimization problem, there are certain constraints that have to be fulfilled. The first constraint 
is shown by Equation (\ref{response_injection_equation}) and the second constraint is shown by Equation (\ref{response_lower_bound}), where the lower bound of the response is determined by $c_n(t)=0$.
The third constraint is the global energy balance
\begin{equation}
\sum_n P_n = 0
\label{global_balance}
\end{equation}
assuming that there are no energy losses. The fourth constraint is the relation between injection pattern and power flow across various nodes in a network. Here we introduce two ways of finding the injection-flow relation.

\subsection{Simplified AC Power Flow}
Although the majority of todays power flow is governed by AC power flow, we can apply the so-called DC approximation for a stable network. We start from solving power flow for AC power flow, which requires two sets of equations
\begin{equation}
P_n = \sum_m|V_n||V_m|(g_{nm}\cos \theta_{nm} + b_{nm}\sin \theta_{nm})
\label{active_power_equation}
\end{equation}
\begin{equation*}
Q_n = \sum_m|V_n||V_m|(g_{nm}\sin \theta_{nm} - b_{nm}\cos \theta_{nm})
\end{equation*}
$Q_n$ denotes the reactive power mismatch. $V_n, g_{nm}, b_{nm}, \theta_{nm}$ refer to voltage level at node $n$, conductance and susceptance between node $n$ and $m$, and the phase angle difference at nodes $n$ and $m$, respectively. We can apply the so-called DC approximation assuming a stable system with no reactive power losses, which yields $Q_n=0$. If we further assume stable voltage levels throughout the grid, zero conductance of the lines, and sufficiently small phase angles, then Equation (\ref{active_power_equation}) becomes
\begin{equation} 
\begin{split}
P_n
& = \sum_m b_{nm} \theta_{nm} \\
& = \sum_m b_{nm} (\theta_n-\theta_m) \\
& = (\sum_m b_{nm}) \theta_n - \sum_m (b_{nm }\theta_m) \\
& = \sum_m B_{nm} \theta_m \\
\end{split}
\label{nodal_active_power}
\end{equation}
where $B_{nm}$ is the nodal susceptance matrix with
\[
B_{nm}=
\begin{cases}
\sum_m b_{nm} & \text{if n=m}\\
-b_{nm} & \text{otherwise}
\end{cases}
\]
The DC approximation also leads to active power flow on a link from node $n$ to $m$
\begin{equation}
F_{n \to m} = b_{nm}(\theta_n - \theta_m)
\label{link_power_flow}
\end{equation}
Equation (\ref{nodal_active_power}) yields the phase angles, which determine the power flow on the links by Equation (\ref{link_power_flow}). The resulting linear relationship between injection pattern and power flow can be expressed using the matrix of Power Transfer Distribution Factors $H_{ln}$ (PTDF matrix)
\begin{equation}
F_l = \sum_n H_{ln} P_n
\label{PTDF_relation}
\end{equation}
where $F_l$ denotes the power flow on the link between nodes $n$ and $m$. The PTDF matrix can be calculated by
\begin{equation}
H_{ln} = \Omega_{ll} K^T_{ln} B^{-1}_{nn}
\label{PTDF}
\end{equation}
$\Omega_{ll}$ is the diagonal matrix with the susceptances on the links $l$, $K^T_{ln}$ is the transposed incidence matrix of the network with the elements
\[
K^T_{ln} =
\begin{cases}
1 & \text{if link l starts at node n} \\
-1 & \text{if link l ends at node n} \\
0 & \text{otherwise}
\end{cases}
\]
And $B^{-1}_{nn}$ is the pseudo inverse of the nodal susceptance matrix $B_{nn}$. For a network with constrained line capacities, we introduce lower and upper bounds for the flow
\begin{equation*}
f^-_l \leq F_l \leq f^+_l
\label{flow_constraint}
\end{equation*}
Then the power flow optimization problem under simplified AC power flow approximation can be described as
\begin{equation*}
\begin{aligned}
& \underset{R_n}{\text{minimize}}
& & \sum_n (R_n-R^0_n)\cdot f(R_n)  \\
& \text{subject to}
& & R_n=P_n+C_n,& \forall n \\
& & & R_n \geq R^0_n(t), \ C_n \geq 0, & \forall n \\
& & & \sum_n P_n = 0 \\
& & & F_l = \sum_n H_{ln} P_n,& \forall l \\
& & & f^-_l \leq F_l \leq f^+_l,& \forall l \\
\end{aligned}
\end{equation*}

\subsection{DC Power Flow}
Equation (\ref{PTDF_relation}) guarantees Kirchhoff's first law and second law, which correspond to energy conservation and phase angle conservation (the sum of phase angles are equal to zero). We can further simplify the injection-flow relation by dropping off the Kirchhoff's second law. In a coarse-grained network, such as a network of country level, it is common that devices to shift the phase angles are installed to control the power flow. So it makes sense that we drop off the Kirchhoff's second law since it violates the phase angle conservation in such case. Then the relation between injection pattern and flow becomes
\begin{equation}
P_n = \sum_l K_{nl} F_l
\label{incidence_relation}
\end{equation}
Equation (\ref{incidence_relation}) makes sure that global energy is balanced, meaning that Equation (\ref{global_balance}) is automatically satisfied
\begin{equation*}
\begin{split}
\sum_n P_n 
& = \sum_n\sum_l K_{nl} F_l \\
& = \sum_l(\sum_n K_{nl}) F_l \\
& = \sum_l (\vec{0})F_l \\
& = 0 \\
\end{split}
\end{equation*}
Similarly, the power flow optimization problem under DC power flow approximation can be described as
\begin{equation*}
\begin{aligned}
& \underset{R_n}{\text{minimize}}
& & \sum_n (R_n-R^0_n)\cdot f(R_n)  \\
& \text{subject to}
& & R_n=P_n+C_n,& \forall n \\
& & & R_n \geq R^0_n(t), \ C_n \geq 0, & \forall n \\
& & & P_n = \sum_l K_{nl} F_l, & \forall n \\
& & & f^-_l \leq F_l \leq f^+_l,& \forall l \\
\end{aligned}
\end{equation*}

\subsection{Extra Cost}
However, in some cases of global excess ($\sum_n R^0_n(t)>0$), there may exist different injection patterns which result in the same objective value. Figure \ref{global_excess} shows an example of global excess at a given time step for a network containing two nodes, 'C' and 'D'. Ideally, both nodes should curtail 1000~MWh locally, and the overall system cost would be zero. However, if node 'C' transfers 1000~MWh to 'D', we also end up with a zero system cost. 

\begin{figure}[H]
	\centering
	\includegraphics[width=0.49\linewidth]{RES_excess1.pdf}
	\includegraphics[width=0.49\linewidth]{RES_excess2.pdf}
	\caption{Marginal cost and nodal response relation for two nodes with renewable excess.}
	\label{global_excess}
\end{figure}

To punish this behavior of transferring the curtailment, we add an extra cost to our objective function
\begin{equation*}
\underset{R_n, F_l}{\text{minimize}} \ \ 
\sum_n (R_n-R^0_n)\cdot f(R_n) + \epsilon \sum_l F^2_l
\end{equation*}
where the sum of quadratic flow is weighted by $\epsilon$. We choose $\epsilon$ such that the extra cost of flow has little effect to the overall system cost
\begin{equation*}
\sum_n (R_n-R^0_n)\cdot f(R_n) \gg \epsilon \sum_l F^2_l
\end{equation*}

\subsection{Simplified Optimization under DC Power Flow}
We can simplify our optimization problem by removing the variables which don't appear in the objective function. $R_n = P_n+C_n$ plus $C_n\geq0$ are equivalent to $R_n\geq P_n$. If the power flow is approximated by DC network, $P_n$ can be replaced by $F_l$ according to Equation (\ref{incidence_relation}), and we end up with $R_n \geq \sum_l K_{nl} F_l$. Then the optimization problem can be simplified as 
\begin{equation*}
\begin{aligned}
& \underset{R_n, F_l}{\text{minimize}}
& & \sum_n (R_n-R^0_n)\cdot f(R_n) + \epsilon \sum_l F^2_l  \\
& \text{subject to}
& & R_n \geq \sum_l K_{nl} F_l, \ \ R_n \geq R^0_n(t), & \forall n \\
& & & f^-_l \leq F_l \leq f^+_l,& \forall l \\
\end{aligned}
\end{equation*}

\section{Code Implementation}
The optimization problem is implemented in python, and solved by gurobi. Gurobi is a state-of-the-art optimization solver for linear programming, quadratic programming and so on. The model is built via gurobipy, a gurobi module which uses python as interface. Besides gurobi, the script leans heavily on some packages, namely numpy, networkx, pandas, and multiprocessing for parallel calculation. In addition, it uses one auxiliary function, namely 'drop\_duplicates\_from\_index', which removes the duplicate index for a pandas series. 

\subsection{Input data}
The topology is defined by parameters 'nodes' and 'edges', which are networkx objects and loaded from 'data/graph' folder. Flow constraint can be added or not, depending on modeling needs. Based on incidence and link susceptance matrices, the PTDF matrix is calculated as in Equation \ref{PTDF}. Then the cost-response data from EnergyPLAN, such as Figure \ref{renewable_deficit} and \ref{renewable_excess}, is stored in country level as a dictionary of pandas dataframe. 


\subsection{Power flow optimization}
The objective function consists of two parts: the power flow cost $\epsilon \sum_l F^2_l$ and the system cost $\sum_n (R_n-R^0_n)\cdot f(R_n)$. The power flow cost is simply a quadratic expression to punish the behavior of transferring curtailment. As Figure \ref{renewable_deficit} shows that $f(R_n)$ is a piecewise linear relation, we need to implement piecewise linear objective function for $R_n$ via gurobi built-in function 'setPWLObj'. There are four decision variables defined as gurobi 'var' objects: $R_n, F_l, P_n$ and $C_n$, namely response value, power flow, injection pattern and curtailment respectively. In principle, only two decision variables are needed, since injection pattern and flow are related Equation \ref{incidence_relation}, and so do response, injection and curtailment Equation \ref{response_injection_equation}. There are three more constraints, namely lower bound of $R_n$ due to zero-cost response value, lower and upper bounds of $F_l$ due to flow constraint, and curtailment can not be negative. 

To achieve high performance of solving the optimization problem, we utilize python's multiprocessing module 'Pool' and parallelize the solving procedure, since we are allowed to treat all the time steps independently. Parallelization by multi-cores reduces the computation time significantly. After the optimization is solved, the decision variables are stored in a pandas dataframe, indexed by time step. In order to feed back into EnergyPLAN, the cost as a function of time for each country is interpolated based on the cost-response curve.

\end{document}
